# -*- coding: utf-8 -*-
import inspect
import logging

from flask import render_template
from flask import request, jsonify, Blueprint
from flask import url_for
from google.appengine.api import taskqueue

from application import queue
from application.hello.models import Counter, COUNTER_KEY
from application.queue.views import bp_queue

bp_hello = Blueprint('hello', __name__, url_prefix='/hello')


@bp_hello.route('/', methods=['GET'])
def home():
    counter = Counter.get_by_id(COUNTER_KEY)
    count = counter.count if counter else 0

    return render_template('index.html', count=count)


@bp_hello.route('/enqueue', methods=['POST'])
def enqueue():
    amount = int(request.form['amount'])

    task = taskqueue.add(method='POST', url=url_for('queue.default_queue'), params={'amount': amount})
    return 'Task {} enqueued, ETA {}.'.format(task.name, task.eta)


@bp_hello.route('/custom_enqueue', methods=['POST'])
def custom_enqueue():
    amount = int(request.form['amount'])

    # task = taskqueue.add(method='POST', url=url_for('queue.default_queue'), params={'amount': amount})
    task = taskqueue.add(method='POST', queue_name='my-task', url=url_for('queue.custom_queue'), params={'amount': amount})
    return 'Task {} enqueued, ETA {}.'.format(task.name, task.eta)

