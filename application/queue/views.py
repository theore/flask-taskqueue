# -*- coding: utf-8 -*-
import inspect
import logging

from flask import render_template
from flask import request, jsonify, Blueprint
from google.appengine.ext import ndb

from application.hello.models import Counter, COUNTER_KEY

bp_queue = Blueprint('queue', __name__, url_prefix='/_ah/queue')


@bp_queue.route('/', methods=['POST'])
def default_queue():
    logging.info('### default_queue')
    amount = request.form['amount']
    counter = Counter.get_or_insert(COUNTER_KEY, count=0)
    counter.count += int(amount)
    counter.put()

    return jsonify(result='ok')


@bp_queue.route('/custom', methods=['POST'])
def custom_queue():
    logging.info('### custom_queue')
    amount = request.form['amount']
    counter = Counter.get_or_insert(COUNTER_KEY, count=0)
    counter.count += int(amount)
    counter.put()
    return jsonify(result='ok')

