# -*- coding: utf-8 -*-
from configration_base import BaseConfig


class EnvironmentConfig(BaseConfig):
    """Staging specific config"""
    DEBUG = True
    TESTING = True
