# -*- coding: utf-8 -*-


class BaseConfig(object):
    """Base config class"""
    SECRET_KEY = 'secret-key'
    DEBUG = True
    TESTING = False
